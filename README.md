# git-snip

[![Version](https://img.shields.io/crates/v/git-snip.svg)](https://crates.io/crates/git-snip)
[![Downloads](https://img.shields.io/crates/d/git-snip.svg)](https://crates.io/crates/git-snip)
[![License](https://img.shields.io/crates/l/git-snip.svg)](https://crates.io/crates/git-snip)
[![Coverage report](https://gitlab.com/radek-sprta/git-snip/badges/main/coverage.svg)](https://gitlab.com/radek-sprta/git-snip/-/commits/main)

git-snip is a CLI utility to delete orphaned local branches that have no upstream.

![](static/animation.svg)

## Installation

You can install git-snip by building it with cargo. Run the following command:

`cargo install --target-dir ~/.local git-snip`

This will create a binary in `~/.local/bin/git-snip`.

## Usage

In a git repository, run:

`git snip`

It will ask to confirm the branches to delete. If you want to delete all branches
without confirmation, run:

`git snip --yes`

To run git-snip automatically, run it with the `--install-hook` flag:

`git snip --install-hook`

## License

GNU General Public License v3.0

# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
  - lint
  - test
  - release

lint:
  before_script:
    - apk add --no-cache bash git just libressl-dev make musl-dev pkgconf py3-pip ruby-dev
    - rustup component add rustfmt
    - rustup component add clippy
    - pip3 install --break-system-packages pre-commit
  image: rust:alpine
  script:
    - just lint
  stage: lint

test:
  artifacts:
    paths:
      - 'coverage'
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
  before_script:
    - apt update
    - apt install -y lcov libssl-dev pkg-config python3-pip
    - rustup component add llvm-tools-preview
    - cargo install grcov
    - pip3 install --break-system-packages lcov_cobertura
  coverage: '/lines\.*: ([\d\.]+%)/'
  image: rust:slim
  script:
    - cargo test
    # generate html report for gitlab pages
    - grcov . --binary-path ./target/debug/ -s . -t html --branch --ignore-not-existing --ignore "*cargo*" -o ./coverage/
    # generate cobertura report for gitlab integration
    - grcov . --binary-path ./target/debug/ -s . -t lcov --branch --ignore-not-existing --ignore "*cargo*" -o coverage.lcov
    - lcov_cobertura coverage.lcov
    # output coverage summary for gitlab parsing
    - lcov --summary coverage.lcov
  stage: test
  variables:
    LLVM_PROFILE_FILE: "coverage-%p-%m.profraw"
    RUSTFLAGS: "-Cinstrument-coverage"

sast:
  stage: test
include:
  - template: Security/SAST.gitlab-ci.yml

release:
  before_script:
    - git checkout "$CI_COMMIT_REF_NAME"
  image:
    entrypoint: [""]
    name: marcoieni/release-plz
  only:
    - main
  script:
    - release-plz --verbose release --backend gitlab --git-token "${GITLAB_TOKEN}" --repo-url https://gitlab.com/radek-sprta/git-snip.git --token "${CARGO_TOKEN}" --allow-dirty
  stage: release

pages:
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  needs:
    - test
  script:
    - cp -r ./coverage ./public
  stage: release

use git2::Repository;

/// Traverse the directory tree to find the repository root. Return
/// Ok(Repository) if found, or Error if not found.
pub fn open() -> Result<Repository, git2::Error> {
    let path = std::env::current_dir().expect("Failed to get current directory.");
    Repository::discover(path)
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::{env, fs};

    use crate::test_utilities;

    #[test]
    fn test_open_current() {
        // GIVEN a repository
        let (_testdir, repo) = test_utilities::create_mock_repo();

        let original_dir = env::current_dir().unwrap();
        env::set_current_dir(repo.path()).unwrap();

        // WHEN the repository is opened
        let actual = open().unwrap();
        env::set_current_dir(original_dir).unwrap();

        // THEN it should match the expected repository
        assert_eq!(actual.path(), repo.path());
    }

    #[test]
    fn test_open_parent() {
        // GIVEN a repository with a subdirectory
        let (_testdir, repo) = test_utilities::create_mock_repo();
        let subdir = repo.path().join("subdir");
        fs::create_dir(&subdir).unwrap();

        let original_dir = env::current_dir().unwrap();
        env::set_current_dir(&subdir).unwrap();

        // WHEN the repository is opened
        let actual = open().unwrap();
        env::set_current_dir(original_dir).unwrap();

        // THEN it should match the expected repository
        assert_eq!(actual.path(), repo.path());
    }

    #[test]
    fn test_open_not_found() {
        // GIVEN a directory that is not a repository
        let testdir = tempfile::tempdir().unwrap();

        let original_dir = env::current_dir().unwrap();
        env::set_current_dir(testdir.path()).unwrap();

        // WHEN the repository is opened
        let actual = open();
        env::set_current_dir(original_dir).unwrap();

        // THEN it should not be found
        assert!(actual.is_err());
    }
}

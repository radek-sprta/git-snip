use std::collections::HashSet;

use git2::Repository;

/// Get a list of remotes for a repository. If there are no remotes, return an
/// empty HashSet.
fn list_remotes(repo: &Repository) -> HashSet<String> {
    let mut remotes = HashSet::new();
    if let Ok(remote_list) = repo.remotes() {
        for remote in remote_list.into_iter().flatten() {
            remotes.insert(remote.to_string());
        }
    }
    remotes
}

/// Get a list of remote prefixes for a repository. If there are no remotes,
/// return an empty HashSet.
pub fn list_remote_prefixes(repo: &Repository) -> HashSet<String> {
    let mut prefixes = HashSet::new();
    for remote in list_remotes(repo) {
        prefixes.insert(as_prefix(remote));
    }
    prefixes
}

/// Return remote as prefix. If it does not end in a slash, add one.
/// e.g. "origin" -> "origin/"
/// e.g. "origin/" -> "origin/"
fn as_prefix(remote: String) -> String {
    if remote.ends_with('/') {
        remote.to_string()
    } else {
        format!("{}/", remote)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_as_prefix() {
        assert_eq!(as_prefix("origin".to_string()), "origin/");
        assert_eq!(as_prefix("origin/".to_string()), "origin/");
    }

    #[test]
    fn test_list_remote_prefixes() {
        // GIVEN a repository with remotes
        let (_testdir, repo) = crate::test_utilities::create_mock_repo();
        let remote = "origin";
        let remote_prefix = format!("{}/", remote);
        repo.remote(remote, "https://example.com").unwrap();

        // WHEN the remote prefixes are listed
        let actual = list_remote_prefixes(&repo);

        // THEN it should match the expected remote prefix
        assert_eq!(actual.len(), 1);
        assert!(actual.contains(&remote_prefix));
    }

    #[test]
    fn test_list_remote_prefixes_empty() {
        // GIVEN a repository without remotes
        let (_testdir, repo) = crate::test_utilities::create_mock_repo();

        // WHEN the remote prefixes are listed
        let actual = list_remote_prefixes(&repo);

        // THEN it should be empty
        assert!(actual.is_empty());
    }
}

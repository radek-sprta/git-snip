use std::io;
use std::io::BufRead;

/// Prints a prompt and reads a line of input.
fn prompt<R>(prompt_text: &str, mut reader: R) -> String
where
    R: BufRead,
{
    println!("{}", prompt_text);
    let mut input = String::new();
    reader.read_line(&mut input).expect("Failed to read input.");
    input.trim().to_lowercase()
}

/// Prints a prompt and reads a line of input from stdin.
pub fn prompt_stdin(prompt_text: &str) -> String {
    prompt(prompt_text, io::stdin().lock())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_prompt() {
        // GIVEN a prompt with expected text
        let prompt_text = "Enter some text:";
        let expected = "hello";
        let reader = std::io::Cursor::new(expected);

        // WHEN the user enters text
        let actual = prompt(prompt_text, reader);

        // THEN it should match the expected one
        assert_eq!(actual, expected);
    }
}

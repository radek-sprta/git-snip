mod branches;
mod hook;
mod input;
mod remote;
mod repo;

use hook::GitHook;

/// Delete all local branches that are not in remote branches.
pub fn run(no_confirm: bool, install_hook: bool) {
    let repo = match repo::open() {
        Ok(repo) => repo,
        Err(e) => {
            eprintln!("Failed to open repository: {}", e);
            return;
        }
    };

    // Install the hook script if requested.
    if install_hook {
        println!("Installing git-snip hook script.");
        let hook = GitHook::default();
        if let Err(e) = hook.install(repo.path()) {
            eprintln!("Failed to install hook: {}", e);
        }
    }

    let branches_to_delete = branches::list_to_delete(&repo);
    if branches_to_delete.is_empty() {
        println!("No local branches to delete.");
        return;
    }

    if !no_confirm {
        println!("Local branches to delete:");
        for b in &branches_to_delete {
            println!("- {}", b);
        }

        let user_input = input::prompt_stdin("Delete these branches? (y/n): ");
        if (user_input != "y") && (user_input != "yes") {
            println!("Aborting.");
            return;
        }
    }

    for b in branches_to_delete {
        branches::delete(&repo, &b).unwrap();
    }
}

#[cfg(test)]
pub mod test_utilities {
    use git2::{Repository, RepositoryInitOptions};
    use tempfile::TempDir;

    /// Create a mock Git repository with initial commit in a temporary
    /// directory for testing.
    pub fn create_mock_repo() -> (TempDir, Repository) {
        let tempdir = TempDir::new().unwrap();
        let mut opts = RepositoryInitOptions::new();
        opts.initial_head("main");
        let repo = Repository::init_opts(tempdir.path(), &opts).unwrap();

        // Create initial commit
        {
            let mut config = repo.config().unwrap();
            config.set_str("user.name", "name").unwrap();
            config.set_str("user.email", "email").unwrap();
            let mut index = repo.index().unwrap();
            let id = index.write_tree().unwrap();

            let tree = repo.find_tree(id).unwrap();
            let sig = repo.signature().unwrap();
            repo.commit(Some("HEAD"), &sig, &sig, "initial\n\nbody", &tree, &[])
                .unwrap();
        }
        (tempdir, repo)
    }
}

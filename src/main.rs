use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about=None)]
struct Cli {
    // Continue without confirmation.
    #[arg(short, long, action)]
    yes: bool,

    // Install post-merge & post-rewrite hook to automatically run git-snip.
    #[arg(short, long, action)]
    install_hook: bool,
}

// Main entry point for git-snip.
fn main() {
    let cli = Cli::parse();
    git_snip::run(cli.yes, cli.install_hook);
}

#[cfg(test)]
#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Cli::command().debug_assert()
}

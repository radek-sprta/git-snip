# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0](https://gitlab.com/radek-sprta/git-snip/releases/tag/v0.1.0) - 2023-12-30

### Added

- add `--yes` option to skip confirmation
- add confirmation dialog to delete branches
- initial version

### Fixed

- exit early when no branches to delete
- fix the library name

### Other

- add just recipe for changelog
- update dependencies
- add test for CLI
- capitalize default confirmation value
- add justfile
- add pre-commit configuration
- update readme
- rename to git-snip
- add unit tests
- Initial commit

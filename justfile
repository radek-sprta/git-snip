set positional-arguments

default:
    @just --list

build:
    cargo build

changelog:
    release-plz update

fmt:
    cargo fmt

lint:
    pre-commit run --all

release:
    cargo publish

run *args:
    cargo run -- {{args}}

test:
    #!/bin/bash
    export RUST_BACKTRACE=1
    cargo test

update:
    cargo update
